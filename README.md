# Github Search Users

A sample React application which searches for a Github user and displays their profile picture with followers count and a list of their followers.

## Development

First time setup:

```
$ nvm use
$ npm install
```

Running local dev server:

```
$ npm run start
```

Tests:

```
$ npm run test
```


export const baseURL = 'https://api.github.com';

/**
 * Performs a request using `fetch`.
 *
 * @param {string} endpoint The endpoint which is appended to the Bold
 * @param {string} method An HTTP method like GET, POST or HEAD.
 * @param {object} [data] Data for methods like POST, PATCH or PUT.
 * @returns {Promise<Response>}
 *
 * @example
 * request({ endpoint: '/users', method: 'GET' })
 *   .then(console.log);
 *
 * @example
 * request({
 *   endpoint :'/users',
 *   method: 'POST',
 *   data: { name: 'Yoda' }
 * }).then(console.log);
 */
async function request(endpoint, method, data) {
  let body;

  if (method === 'POST')
    body = { ...data };

  const url = baseURL + endpoint;

  const res = await fetch(url, {
    method,
    headers: {},
    body,
  });

  return await res.json();
}

export { request };

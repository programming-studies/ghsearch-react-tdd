import { request } from './api';

describe('request()', () => {
  it('should make a GET request to the given endpoint', async () => {
    global.fetch = jest.fn(() => {
      return Promise.resolve({
        json: () => Promise.resolve({ hello: 'world' }),
      });
    });

    const res = await request('/foo', 'GET');

    expect(fetch.mock.lastCall).toEqual([
      'https://api.github.com/foo',
      expect.objectContaining({ method: 'GET' }),
    ]);

    expect(res).toEqual({ hello: 'world' });
  });

  it('should make a POST request to a given endpoint', () => {
    global.fetch = jest.fn(() => {
      return Promise.resolve({
        json: () => Promise.resolve(),
      });
    });

    request('/jedi', 'POST', { name: 'Yoda' });

    expect(fetch.mock.lastCall).toEqual([
      'https://api.github.com/jedi',
      expect.objectContaining({
        method: 'POST',
        body: expect.objectContaining({ name: 'Yoda' }),
      }),
    ]);
  });
});

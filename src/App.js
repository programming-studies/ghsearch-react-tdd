import React, { useState } from 'react';
import { SearchForm } from "./components/SearchForm";

function App() {
  const [searchTerm, setSearchTerm] = useState("");

  return (
    <div className="app-root">
      <p>It works!</p>
      <SearchForm searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
    </div>
  );
}

export default App;

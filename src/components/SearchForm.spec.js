import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { SearchForm } from './SearchForm';

describe('<SearchForm />', () => {
  it('should render text field', () => {
    render(<SearchForm />);

    expect(
      screen.getByRole('textbox', {
        name: /github user to search/i
      })
    ).toBeInTheDocument();
  });

  it('should render a submit button', () => {
    render(<SearchForm />);

    expect(
      screen.getByRole('button', {
        name: /search/i
      })
    ).toBeInTheDocument();
  });

  it('should have submit button disabled if no search term exists', () => {
    render(<SearchForm />);

    expect(
      screen.getByRole('button', {
        name: /search/i
      })
    ).toBeDisabled();
  });

  it('should have submit button disabled if search term is less than 2 chars', () => {
    render(<SearchForm searchTerm='a' />);

    expect(
      screen.getByRole('button', {
        name: /search/i
      })
    ).toBeDisabled();
  });

  it('should enable button when search term is two-or-more chars', () => {
    render(<SearchForm searchTerm="ab" />)

    expect(
      screen.getByRole('button', {
        name: /search/i
      })
    ).toBeEnabled();
  });

  it('should set search term when user types query', () => {
    const spySetSearchTerm = jest.fn();

    render(
      <SearchForm
        setSearchTerm={spySetSearchTerm}
      />
    );

    const textField = screen.getByRole('textbox', {
      name: /github user to search/i,
    });

    fireEvent.input(textField, { target: { value: 'li' } });

    expect(spySetSearchTerm).toHaveBeenCalledWith('li');
  });
});


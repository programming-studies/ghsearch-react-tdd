import React from 'react';

function shouldDisable(searchTerm) {
  return typeof searchTerm !== 'string' || searchTerm.length < 2;
}

function SearchForm({
  searchTerm,
  setSearchTerm,
}) {
  return (
    <form method="get">
      <p>
        <label id="search-term">Github user to search:</label>
        <input
          type="text"
          id="search-term"
          aria-labelledby="search-term"
          onChange={e => setSearchTerm(e.target.value)}
        />
      </p>

      <p>
        <button type="submit" disabled={shouldDisable(searchTerm)}>Search</button>
      </p>
    </form>
  );
}

export { SearchForm };

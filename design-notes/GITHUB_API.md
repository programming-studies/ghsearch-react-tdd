# Github API

- [Github API](#github-api)
  - [Testing with cURL](#testing-with-curl)
    - [Zen endpoint](#zen-endpoint)
    - [Users endpoint](#users-endpoint)
  - [References](#references)

## Testing with cURL

### Zen endpoint

```
$ curl --head https://api.github.com/zen
...
x-ratelimit-limit: 60
x-ratelimit-remaining: 58
x-ratelimit-reset: 1661251941
x-ratelimit-resource: core
x-ratelimit-used: 2
...
```

With auth token:

```
$ curl --head \
    --header 'Authorization: token <TOKEN>' \
    https://api.github.com/zen
...
x-ratelimit-limit: 5000
x-ratelimit-remaining: 4997
x-ratelimit-reset: 1661251713
x-ratelimit-used: 3
...
```

### Users endpoint

Simple:

```
$ curl https://api.github.com/users
```

With auth token:

```
$ curl \
    --header "Accept: application/vnd.github+json" \ 
    --header "Authorization: token <TOKEN>" \
    https://api.github.com/users
```

We mostly need:

- `login`
- `html_url` (link to their profile)
- `avatar_url`
- `followers` (a number)

## References
- [Github API uses docs](https://docs.github.com/en/rest/users/users).
- [Github settings page manage tokens](https://github.com/settings/tokens) (must be logged in).
